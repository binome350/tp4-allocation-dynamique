#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <assert.h>

#include "mem.h"

int main()
{
    // initialize the allocator
    mem_init();

    // request a block of 42 bytes
    char * b = mem_alloc( 42 ); 

    // check that the allocation was successful
    assert( b != NULL );
    printf("allocated 42 bytes at %p\n", b);

    
    // request another block 
    char *c = mem_alloc( 100 );

    assert( c != NULL );
    printf("allocated 100 bytes at %p\n", c);
    

    return 0;
}
