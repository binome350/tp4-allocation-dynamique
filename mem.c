#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <assert.h>

#include "mem.h"

// metadata at the beginning of each memory chunk
// (the linking pointers are only meaningful for free chunks)

typedef struct _h {
    int64_t size; //the number of bytes in this chunk (header included)
    struct _h *next; // linked list pointer
    struct _h *prev; // linked list pointer
} header_t;

// a pointer to the first free chunk in the heap.
static header_t *freelist_head = NULL;

// the area we are going to allocate from
static char heap[1024];

// a boolean flag indicating whether we are operational
static int mem_initialized = 0;

// display the freelist in readable form
void mem_print_freelist();

// display just a chunk
void mem_print_thisChunk(header_t *chunkToPrint);

// fusion the free space in memory
void mem_consolidate();

// initialize the memory manager

void mem_init(void) {
    assert(mem_initialized == 0);

    freelist_head = (header_t*) heap;

    freelist_head->next = NULL;
    freelist_head->prev = NULL;
    freelist_head->size = sizeof (heap);

    mem_initialized = 1;
}

// rounds x up to the nearest multiple of 8, e.g. roundup8(14)=16

int64_t roundup8(int64_t x) {
    assert(x >= 0); // sanity check

    if (x % 8 == 0) // check if x is already a multiple of 8
    {
        return x;
    }

    return 8 * (1 + x / 8);
}

// allocate a region of memory and return its address

void * mem_alloc(int64_t size) {
    assert(mem_initialized == 1);

    printf(" ----------------------------\n");
    printf(" DEBUG BEFORE ALLOCATING MEMORY = %ld for data, %ld for header\n", size, sizeof (header_t));
    mem_print_freelist();
    printf(" ----------------------------\n");

    // =========================== TESTS ===========================
    // if we have no free chunk then we have nothing to do
    if (freelist_head == NULL) {
        printf("> ERROR : No free space, no freelist !\n");
        return NULL;
    }

    // the chunk will have to contain both header and user data
    int64_t needed = roundup8(size + sizeof (header_t));
    // The chunk that will contain the data.
    header_t *chunk = freelist_head;

    //Want to find a valid chunk
    while (chunk != NULL && chunk->size < needed) {
        chunk = chunk->next;
    }

    //No large enough chunk was found
    if (chunk == NULL) {
        printf("> ERROR : No large enough chunk found !\n");
        return NULL;
    }

    // We have a large enough chunk, avaible. Let's cut it.

    //Random declarations to prevent errors 
    int64_t dataptr_as_int;
    void * dataptr;

    // Does the chunk without the needed space can be use later ? 
    if (chunk->size - needed <= sizeof (header_t)) { //If no
        printf("> No free space left, we use a whole chunk.\n");
        //we use the whole chunk
        int64_t chunkptr_as_int = (int64_t) chunk;
        dataptr_as_int = chunkptr_as_int + sizeof (header_t); //we jump over the header
        dataptr = (void*) dataptr_as_int;

        //We want to extract the chunk, so we have to do some plumbing
        header_t *chunkBEFORE = chunk->prev;
        header_t *chunkAFTER = chunk->next;

        chunk->next = NULL;
        chunk->prev = NULL;

        if (chunkBEFORE != NULL) { //prevent some "problems" of ptr
            chunkBEFORE->next = chunkAFTER;
        }
        if (chunkAFTER != NULL) { //prevent some "problems" of ptr
            chunkAFTER->prev = chunkBEFORE;
        }

        //if it's the freelist we just give to store some data, we have no more freelist !
        if (chunk == freelist_head && chunkAFTER == NULL) {
            freelist_head = NULL;
        } else if (chunk == freelist_head) { //If we just take the head of the freelist, we have to reconnect it
            freelist_head = chunkAFTER;
        }

        //Just a cast to be more readable for human :)
        header_t* elementEnCoursRetourned = (header_t*) chunkptr_as_int;
        //DEBUG // 
        mem_print_thisChunk(elementEnCoursRetourned);

    } else { // if yes
        printf("> Free space left, we slice the chunk.\n");

        //modify the chunk to create a smaller chunk
        chunk->size = chunk->size - needed;

        int64_t chunkptr_as_int = (int64_t) chunk + chunk->size; // we jump over the free space to get a ptr to the "good" zone
        //Just a cast to be more readable for human :)
        header_t* elementEnCoursRetourned = (header_t*) chunkptr_as_int;
        elementEnCoursRetourned->size = needed; //we put the size of the cunk removed to permit to get it back one day.
        elementEnCoursRetourned->next = NULL;
        elementEnCoursRetourned->prev = NULL;

        //DEBUG // 
        mem_print_thisChunk(elementEnCoursRetourned);

        dataptr_as_int = chunkptr_as_int + sizeof (header_t); //we jump over the header
        dataptr = (void*) dataptr_as_int;

        //we create links
    }

    printf(" ----------------------------\n");
    printf(" DEBUG AFTER ALLOCATING MEMORY\n");
    mem_print_freelist();
    printf(" ----------------------------\n");

    return dataptr;
}

// deallocate a chunk of memory

void mem_release(void* ptr) {
    assert(mem_initialized == 1);

    printf(" ----------------------------\n");
    printf(" DEBUG BEFORE RELEASING MEMORY\n");
    mem_print_freelist();
    printf(" ----------------------------\n");

    //we just get the "data" ptr, so we want the "header" pointer. We have to translat the ptr.
    header_t* ptrHeader = (header_t*) ((int64_t) ptr - sizeof (header_t));
    //DEBUG // mem_print_thisChunk(ptrHeader);

    //Does the free list exist ? 
    if (freelist_head == NULL) { //if no
        freelist_head = ptrHeader; //size,and informations are already in the list

        printf("> DEBUG : the size of the freelist is now : %ld\n", freelist_head->size);

        //A little bit of cleaning
        freelist_head->next = NULL;
        freelist_head->prev = NULL;
    } else { // if yes
        //Let's keep a sorted FreeList
        // The chunk that will contain the data.
        header_t *chunkHeader = freelist_head;

        //Does the actual released ptr is before the beginning of the freelist ?
        if (ptrHeader < freelist_head) { //if yes
            //we add the released chunk to the begining of the freelist
            ptrHeader->next = freelist_head;
            ptrHeader->prev = NULL;
            freelist_head->prev = ptrHeader;
            freelist_head = ptrHeader; //The new head is the released chunk

        } else { ////if it's just randomly inside the freelist or at the end

            //Want to find the right chunk just before the adress we release (or the last chunk if at the end)
            while (chunkHeader->next != NULL && chunkHeader->next < ptrHeader) {
                chunkHeader = chunkHeader->next;
            }

            //useful to keep to make some plumbing
            header_t *chunkNextHeader = chunkHeader->next;

            //We have the right (or ending) chunk, we just do some plumbing
            chunkHeader->next = ptrHeader;
            ptrHeader->prev = chunkHeader;
            ptrHeader->next = chunkNextHeader;

            if (chunkNextHeader != NULL) {//Let's do the plumbing more if we are in the middle
                chunkNextHeader->prev = ptrHeader;
            }
        }
    }

    printf(" ----------------------------\n");
    printf(" DEBUG AFTER RELEASING MEMORY\n");
    mem_print_freelist();
    printf(" ----------------------------\n");

    mem_consolidate();

    return;
}

// print the contents of the freelist

void mem_print_freelist() {
    // the four spaces help distinguish us from regular printf's
    printf("    freelist_head=%p\n", freelist_head);

    header_t *chunk = freelist_head;
    while (chunk != NULL) {
        printf("    free chunk %p (prev=%p, next=%p, size=%ld)\n",
                chunk,
                chunk->prev,
                chunk->next,
                chunk->size);

        assert(chunk->size != 0); // sanity check

        chunk = chunk->next;
    }
}

void mem_consolidate() {
    header_t *chunk = freelist_head;

    printf(" ----------------------------\n");
    printf(" DEBUG BEFORE CONSOLIDATING MEMORY\n");
    mem_print_freelist();
    printf(" ----------------------------\n");

    //Want to find two valid contiguous valid chunks
    while (chunk != NULL && chunk->next != NULL) {
        //Just a cast to be more readable for human :)
        header_t *chunkNext = chunk->next;
        header_t *chunkNextNext = chunk->next;

        // Are these chunks contigous ? 
        if ((int64_t) chunkNext == (int64_t) chunk + chunk->size) { //if yes
            //Let's fusion the two chunks
            chunk->size = chunk->size + chunkNext->size;
            //Let's do some plumbing
            chunk->next = chunkNext->next;

            if (chunkNextNext != NULL) {
                chunkNextNext->prev = chunk;
            }
            chunkNext->next = NULL;
            chunkNext->prev = NULL;
        } else {
            chunk = chunkNext;
        }
    }

    printf(" ----------------------------\n");
    printf(" DEBUG AFTER CONSOLIDATING MEMORY\n");
    mem_print_freelist();
    printf(" ----------------------------\n");

}

void mem_print_thisChunk(header_t *chunkToPrint) {
    if (chunkToPrint != NULL) {
        printf("     chunk %p (prev=%p, next=%p, size=%ld)\n",
                chunkToPrint,
                chunkToPrint->prev,
                chunkToPrint->next,
                chunkToPrint->size);
    } else {
        printf("    NULL chunk \n");

    }
}

