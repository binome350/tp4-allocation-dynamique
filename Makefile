
all: user

user: mem.c user.c
	gcc -Werror=all mem.c user.c -o user

clean:
	rm -f *.o user
