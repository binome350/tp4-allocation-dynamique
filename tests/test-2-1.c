#include <stdio.h>
#include <assert.h>

#include "mem.h"

int main()
{
    mem_init();

    char * a = mem_alloc( 300 );
    char * b = mem_alloc( 300 );
    char * c = mem_alloc( 300 );

    assert(a != NULL);
    assert(b != NULL);
    assert(c != NULL);

    char * d = mem_alloc( 300 );
    assert( d == NULL );

    mem_release(b);
    
    char *e = mem_alloc( 300 );
    
    assert( e != NULL );
    assert( e == b);
    
    printf(__FILE__" OK\n");
    
    return 0;
}
