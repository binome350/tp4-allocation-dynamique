#!/bin/sh

# This script runs each test case sequentially, and stops on the first failure.

make --silent clean

for filename in test*.c
do
    # remove the .c suffix to get the program name, cf [1]
    progname=${filename%.c}

    # build program silently, but abort on build errors, cf [2]
    make --silent $progname || exit 1

    # run program silently, cf [3]
    ./$progname >/dev/null || exit 1

    echo $filename OK
done

# for more info about the shell command language:
# http://pubs.opengroup.org/onlinepubs/9699919799/utilities/V3_chap02.html
#
# [1] word manipulation:  http://pubs.opengroup.org/onlinepubs/9699919799/utilities/V3_chap02.html#tag_18_06_02_01
# [2] command lists:      http://pubs.opengroup.org/onlinepubs/9699919799/utilities/V3_chap02.html#tag_18_09_03
# [3] output redirection: http://pubs.opengroup.org/onlinepubs/9699919799/utilities/V3_chap02.html#tag_18_07
