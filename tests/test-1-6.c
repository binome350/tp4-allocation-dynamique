#include <stdio.h>
#include <assert.h>

#include "mem.h"

int main()
{
    mem_init(); 

    int i;
    for(i=0; i< 31; i++) // 31 blocs of 32 bytes each is 992 bytes
    {
        char *a=mem_alloc(1); // 1 byte + header is 32 bytes :-)
        assert( a != NULL );
    }

    // heap should be almost full at this point
    char *b=mem_alloc(9);
    assert( b == NULL );

    char *c=mem_alloc(8);
    assert( c != NULL );

    printf(__FILE__": OK\n");
    return 0;
}
