#include <stdio.h>
#include <assert.h>

#include "mem.h"

int main()
{
    mem_init(); 

    char * a = mem_alloc( 42 );
    assert( a != NULL );

    char * b = mem_alloc( 100 );
    assert( b != NULL );

    
    printf(__FILE__": OK\n");
    return 0;
}
