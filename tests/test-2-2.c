#include <stdio.h>
#include <assert.h>

#include "mem.h"

int main()
{
    mem_init();

    char * a = mem_alloc( 300 );
    char * b = mem_alloc( 300 );
    char * c = mem_alloc( 300 );

    mem_release(c);
    mem_release(b);
    mem_release(a);

    a = mem_alloc( 200 );
    b = mem_alloc( 200 );
    c = mem_alloc( 200 );

    char *d = mem_alloc( 10 );

    assert ( d != NULL );
    
    printf(__FILE__": OK\n");
    
    return 0;
}
