#include <stdio.h>
#include <assert.h>

#include "mem.h"

int main()
{
    mem_init();

    char * a = mem_alloc( 42 );
    char * b = mem_alloc( 42 );
    char * c = mem_alloc( 42 );

    assert(a != NULL);
    assert(b != NULL);
    assert(c != NULL);

    assert( a != b );
    assert( a != c );
    assert( b != c );

    printf(__FILE__": OK\n");
    
    return 0;
}
