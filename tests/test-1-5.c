#include <stdio.h>
#include <assert.h>

#include "mem.h"

int main()
{
    mem_init(); 

    // heap is 1024B
    // granularity is 24B -> usable heap is 1008 B
    // 1008B is just enough for 984B of data + 24B of header

    char * a = mem_alloc( 1001 );
    assert( a == NULL );

    a = mem_alloc( 1000 );
    assert( a != NULL );

    printf(__FILE__": OK\n");
    return 0;
}
