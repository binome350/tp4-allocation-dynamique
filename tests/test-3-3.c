#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <assert.h>

#include "mem.h"

int main()
{
    mem_init();

    char * b0 = mem_alloc( 15 );
    char * b1 = mem_alloc( 15 );
    char * b2 = mem_alloc( 15 );
    char * b3 = mem_alloc( 15 );
    char * b4 = mem_alloc( 15 );
    char * b5 = mem_alloc( 15 );
    char * b6 = mem_alloc( 15 );
    char * b7 = mem_alloc( 15 );
    char * b8 = mem_alloc( 15 );
    char * b9 = mem_alloc( 15 );

    assert( b0 != NULL );
    assert( b1 != NULL );
    assert( b2 != NULL );
    assert( b3 != NULL );
    assert( b4 != NULL );
    assert( b5 != NULL );
    assert( b6 != NULL );
    assert( b7 != NULL );
    assert( b8 != NULL );
    assert( b9 != NULL );

    // then we make sure to exhaust the heap so that the freelist is empty at some point
    char * tmp;
    do {
        tmp = mem_alloc( 1 );
    }while(tmp != NULL);

    mem_release(b1);
    mem_release(b7);
    mem_release(b3);
    mem_release(b9);
    mem_release(b5);
    mem_release(b0);
    mem_release(b6);
    mem_release(b2);
    mem_release(b8);
    mem_release(b4);
    
    mem_alloc(150);
    
    printf(__FILE__": OK\n");
    return 0;
}
