#include <stdio.h>
#include <assert.h>

#include "mem.h"

int main()
{
    mem_init();

    // first we allocate many blocks (they should be side by side)
    char * b0 = mem_alloc( 15 );
    char * b1 = mem_alloc( 15 );
    char * b2 = mem_alloc( 15 );
    char * b3 = mem_alloc( 15 );
    char * b4 = mem_alloc( 15 );
    char * b5 = mem_alloc( 15 );
    char * b6 = mem_alloc( 15 );
    char * b7 = mem_alloc( 15 );
    char * b8 = mem_alloc( 15 );
    char * b9 = mem_alloc( 15 );
    char * ba = mem_alloc( 15 );

    // then we make sure to exhaust the heap
    char * tmp;
    do {
        tmp = mem_alloc( 1 );
    }while(tmp != NULL);

    // then we free half of our numbered blocks, in somewhat arbitrary order
    //
    // the other half remain in use, which prevents any block fusion
    mem_release(b1);
    mem_release(b7);
    mem_release(b3);
    mem_release(b9);
    mem_release(b5);
    // at this point the freelist should be just those five blocks, ordered by address 

    // here is the actual test: we check that first-fit picks the five blocks in the right order
    b1 = mem_alloc( 15 );
    b3 = mem_alloc( 15 );
    b5 = mem_alloc( 15 );
    b7 = mem_alloc( 15 );
    b9 = mem_alloc( 15 );

    assert( b3 > b1 );
    assert( b5 > b3 );
    assert( b7 > b5 );
    assert( b9 > b7 );
    // end of test
    
    // suppress compiler warnings about unused variables
    b0++; b1++; b2++; b3++; b4++; b5++; b6++; b7++; b8++; b9++; ba++;
    
    printf(__FILE__": OK\n");
    return 0;
}
