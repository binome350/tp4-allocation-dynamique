#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <assert.h>

#include "mem.h"

int main()
{
    mem_init();

    char * a = mem_alloc( 300 );
    char * b = mem_alloc( 300 );
    char * c = mem_alloc( 300 );
    char * d = mem_alloc( 300 );

    assert(a != NULL);
    assert(b != NULL);
    assert(c != NULL);

    assert( a != b );
    assert( a != c );
    assert( b != c );

    assert( d == NULL );

    printf(__FILE__" OK\n");
    
    return 0;
}
