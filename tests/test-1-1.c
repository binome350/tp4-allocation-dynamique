#include <stdio.h>
#include <assert.h>

#include "mem.h"

int main()
{
    mem_init(); 

    char * a = mem_alloc( 30 );

    assert( a != NULL );
    
    printf(__FILE__": OK\n");
    return 0;
}
