#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <assert.h>

#include "mem.h"

int main()
{
    mem_init();

    char * a = mem_alloc( 300 );
    char * b = mem_alloc( 300 );
    char * c = mem_alloc( 300 );

    mem_release(b);

    char* d = mem_alloc( 400 );
    assert ( d == NULL );

    mem_release(c);

    d = mem_alloc( 400 );
    assert ( d != NULL );

    a++; // suppress compiler warning about unused variable

    printf(__FILE__": OK\n");
    
    return 0;
}
